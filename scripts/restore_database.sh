#! /bin/bash

# This solution created from the SO post:
# https://stackoverflow.com/a/23778599/3255269
# This needs to be run from the directory with the tarred gzipped archive

# docker create -v /data --name nibrs_backup busybox true

# docker run \
#     --rm \
#     --volumes-from nibrs_backup \
#     -v $(pwd):/backup nibrs_cde_pg_db_1 \
#     tar xvf /backup/nibrs_backup.tar /var/lib/postgresql/data

psql -U postgres -p 5432 -f tmp/nibrs_dump.sql postgres &> pg_restore_log.txt