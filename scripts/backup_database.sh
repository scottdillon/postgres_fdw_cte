#! /bin/bash
# This solution created from the SO post:
# https://stackoverflow.com/a/23778599/3255269

docker run \
    --rm \
    --volumes-from nibrs_cde_pg_db_1 \
    -v $(pwd):/backup busybox \
    tar cvf /backup/nibrs_backup_2.tar /var/lib/postgresql/data

pg_dumpall --clean --file=nibrs_dump.sql -h localhost -l postgres -p 5432 -U postgres