# Window Functions

## What are Window Functions?
Window functions apply a function, possibly an aggregating function, without collapsing the rows of the window into a single value like a `GROUP BY` would.

[Great Window Function Tutorial](http://www.postgresqltutorial.com/postgresql-window-function/)
[CTEs and Window Functions](https://engineeringblog.yelp.com/2015/01/title-ctes-and-window-functions-unleashing-the-power-of-redshift.html)

## Window Function Syntax

```sql
window_function(arg1, arg2,..) OVER (PARTITION BY expression ORDER BY expression)
```

## Anatomy of a Window Function
- `OVER` - Defines the set of rows over which the function applies.
- `PARTITION BY` - Analagous to the `GROUP BY` in an aggregate query.
- `ORDER BY` - Important for a non-aggregating function as it determines the order of data in the window and hence the way the window function is applied.
