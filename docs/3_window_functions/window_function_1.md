# Example 1

## Query
```sql
select
    symbol
   ,date_time
   ,"open"
   ,"close"
   ,FIRST_VALUE ("open") OVER (partition by
   		date_part('year', date_time)
   	   ,date_part('month', date_time)
   	order by date_time) as "Month Open"
   ,LAST_VALUE ("close") over (partition by
   		date_part('year', date_time)
   	   ,date_part('month', date_time)
   	   order by date_time) as "Month Close"
from crypto_prices
where volume > 0 and symbol = 'BTC'
order by date_time desc;
```

## Results

<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg .tg-0lax{text-align:left;vertical-align:top}
</style>
<table class="tg">
  <tr>
    <th class="tg-0lax">symbol</th>
    <th class="tg-0lax">date_time</th>
    <th class="tg-0lax">open</th>
    <th class="tg-0lax">close</th>
    <th class="tg-0lax">Month Open</th>
    <th class="tg-0lax">Month Close</th>
  </tr>
  <tr>
    <td class="tg-0lax">BTC</td>
    <td class="tg-0lax">2018-08-04</td>
    <td class="tg-0lax">7,438.6700</td>
    <td class="tg-0lax">7,032.8500</td>
    <td class="tg-0lax">7,769.0400</td>
    <td class="tg-0lax">7,032.8500</td>
  </tr>
  <tr>
    <td class="tg-0lax">BTC</td>
    <td class="tg-0lax">2018-08-03</td>
    <td class="tg-0lax">7,562.1400</td>
    <td class="tg-0lax">7,434.3900</td>
    <td class="tg-0lax">7,769.0400</td>
    <td class="tg-0lax">7,434.3900</td>
  </tr>
  <tr>
    <td class="tg-0lax">BTC</td>
    <td class="tg-0lax">2018-08-02</td>
    <td class="tg-0lax">7,634.1900</td>
    <td class="tg-0lax">7,567.1500</td>
    <td class="tg-0lax">7,769.0400</td>
    <td class="tg-0lax">7,567.1500</td>
  </tr>
  <tr>
    <td class="tg-0lax">BTC</td>
    <td class="tg-0lax">2018-08-01</td>
    <td class="tg-0lax">7,769.0400</td>
    <td class="tg-0lax">7,624.9100</td>
    <td class="tg-0lax">7,769.0400</td>
    <td class="tg-0lax">7,624.9100</td>
  </tr>
  <tr>
    <td class="tg-0lax">BTC</td>
    <td class="tg-0lax">2018-07-31</td>
    <td class="tg-0lax">8,181.2000</td>
    <td class="tg-0lax">7,780.4400</td>
    <td class="tg-0lax">6,411.68017578</td>
    <td class="tg-0lax">7,780.4400</td>
  </tr>
  <tr>
    <td class="tg-0lax">BTC</td>
    <td class="tg-0lax">2018-07-30</td>
    <td class="tg-0lax">8,221.5800</td>
    <td class="tg-0lax">8,180.4800</td>
    <td class="tg-0lax">6,411.68017578</td>
    <td class="tg-0lax">8,180.4800</td>
  </tr>
  <tr>
    <td class="tg-0lax">BTC</td>
    <td class="tg-0lax">2018-07-29</td>
    <td class="tg-0lax">8,205.8203125</td>
    <td class="tg-0lax">8,218.45996094</td>
    <td class="tg-0lax">6,411.68017578</td>
    <td class="tg-0lax">8,218.45996094</td>
  </tr>
  <tr>
    <td class="tg-0lax">BTC</td>
    <td class="tg-0lax">2018-07-28</td>
    <td class="tg-0lax">8,169.06005859</td>
    <td class="tg-0lax">8,192.15039062</td>
    <td class="tg-0lax">6,411.68017578</td>
    <td class="tg-0lax">8,192.15039062</td>
  </tr>
  <tr>
    <td class="tg-0lax">BTC</td>
    <td class="tg-0lax">2018-07-27</td>
    <td class="tg-0lax">7,950.39990234</td>
    <td class="tg-0lax">8,165.00976562</td>
    <td class="tg-0lax">6,411.68017578</td>
    <td class="tg-0lax">8,165.00976562</td>
  </tr>
  <tr>
    <td class="tg-0lax">BTC</td>
    <td class="tg-0lax">2018-07-26</td>
    <td class="tg-0lax">8,176.85009766</td>
    <td class="tg-0lax">7,951.58007812</td>
    <td class="tg-0lax">6,411.68017578</td>
    <td class="tg-0lax">7,951.58007812</td>
  </tr>
  <tr>
    <td class="tg-0lax">BTC</td>
    <td class="tg-0lax">2018-07-25</td>
    <td class="tg-0lax">8,379.66015625</td>
    <td class="tg-0lax">8,181.39013672</td>
    <td class="tg-0lax">6,411.68017578</td>
    <td class="tg-0lax">8,181.39013672</td>
  </tr>
  <tr>
    <td class="tg-0lax">BTC</td>
    <td class="tg-0lax">2018-07-24</td>
    <td class="tg-0lax">7,716.50976562</td>
    <td class="tg-0lax">8,424.26953125</td>
    <td class="tg-0lax">6,411.68017578</td>
    <td class="tg-0lax">8,424.26953125</td>
  </tr>
  <tr>
    <td class="tg-0lax">BTC</td>
    <td class="tg-0lax">2018-07-23</td>
    <td class="tg-0lax">7,414.70996094</td>
    <td class="tg-0lax">7,711.10986328</td>
    <td class="tg-0lax">6,411.68017578</td>
    <td class="tg-0lax">7,711.10986328</td>
  </tr>
  <tr>
    <td class="tg-0lax">BTC</td>
    <td class="tg-0lax">2018-07-22</td>
    <td class="tg-0lax">7,417.79980469</td>
    <td class="tg-0lax">7,418.49023438</td>
    <td class="tg-0lax">6,411.68017578</td>
    <td class="tg-0lax">7,418.49023438</td>
  </tr>
  <tr>
    <td class="tg-0lax">BTC</td>
    <td class="tg-0lax">2018-07-21</td>
    <td class="tg-0lax">7,352.72021484</td>
    <td class="tg-0lax">7,419.29003906</td>
    <td class="tg-0lax">6,411.68017578</td>
    <td class="tg-0lax">7,419.29003906</td>
  </tr>
</table>