# Window Functions

## [Available Window Functions](https://www.postgresql.org/docs/current/static/functions-window.html)

- `ROW NUMBER`
- `RANK`
- `DENSE RANK` - No ranks are skipped.
- `FIRST_VALUE`
- `LAST_VALUE`
- `PERCENT_RANK` - computes the fraction of partition rows that are less than the current row, assuming the current row does not exist in the partition.
- `CUME_DIST` - computes the fraction of partition rows that are less than or equal to the current row and its peers
- `ntile()`
