# Example 2 Refined

## Query

```sql
with peak_days as (
select
    symbol
   ,date_time
   ,"open"
   ,"close"
   ,rank() over (partition by symbol order by market_cap desc) as day_rank
from crypto_prices
)
select symbol, date_time, "open", "close", day_rank
from peak_days
where day_rank < 11 and symbol in ('BTC', 'ETH', 'XLM', 'XRP');
```

## Results

<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg .tg-0lax{text-align:left;vertical-align:top}
</style>
<table class="tg">
  <tr>
    <th class="tg-0lax">symbol</th>
    <th class="tg-0lax">date_time</th>
    <th class="tg-0lax">open</th>
    <th class="tg-0lax">close</th>
    <th class="tg-0lax">high</th>
    <th class="tg-0lax">day_rank</th>
  </tr>
  <tr>
    <td class="tg-0lax">BTC</td>
    <td class="tg-0lax">2017-12-17</td>
    <td class="tg-0lax">19,475.8007812</td>
    <td class="tg-0lax">19,140.8007812</td>
    <td class="tg-0lax">20,089.0000</td>
    <td class="tg-0lax">1</td>
  </tr>
  <tr>
    <td class="tg-0lax">BTC</td>
    <td class="tg-0lax">2017-12-16</td>
    <td class="tg-0lax">17,760.3007812</td>
    <td class="tg-0lax">19,497.4003906</td>
    <td class="tg-0lax">19,716.6992188</td>
    <td class="tg-0lax">2</td>
  </tr>
  <tr>
    <td class="tg-0lax">BTC</td>
    <td class="tg-0lax">2017-12-18</td>
    <td class="tg-0lax">19,106.4003906</td>
    <td class="tg-0lax">19,114.1992188</td>
    <td class="tg-0lax">19,371.0000</td>
    <td class="tg-0lax">3</td>
  </tr>
  <tr>
    <td class="tg-0lax">BTC</td>
    <td class="tg-0lax">2017-12-19</td>
    <td class="tg-0lax">19,118.3007812</td>
    <td class="tg-0lax">17,776.6992188</td>
    <td class="tg-0lax">19,177.8007812</td>
    <td class="tg-0lax">4</td>
  </tr>
  <tr>
    <td class="tg-0lax">BTC</td>
    <td class="tg-0lax">2017-12-08</td>
    <td class="tg-0lax">17,802.9003906</td>
    <td class="tg-0lax">16,569.4003906</td>
    <td class="tg-0lax">18,353.4003906</td>
    <td class="tg-0lax">5</td>
  </tr>
  <tr>
    <td class="tg-0lax">ETH</td>
    <td class="tg-0lax">2018-01-13</td>
    <td class="tg-0lax">1,270.4699707</td>
    <td class="tg-0lax">1,396.42004395</td>
    <td class="tg-0lax">1,432.88000488</td>
    <td class="tg-0lax">1</td>
  </tr>
  <tr>
    <td class="tg-0lax">ETH</td>
    <td class="tg-0lax">2018-01-10</td>
    <td class="tg-0lax">1,300.33996582</td>
    <td class="tg-0lax">1,255.81994629</td>
    <td class="tg-0lax">1,417.38000488</td>
    <td class="tg-0lax">2</td>
  </tr>
  <tr>
    <td class="tg-0lax">ETH</td>
    <td class="tg-0lax">2018-01-14</td>
    <td class="tg-0lax">1,397.47998047</td>
    <td class="tg-0lax">1,366.77001953</td>
    <td class="tg-0lax">1,400.56005859</td>
    <td class="tg-0lax">3</td>
  </tr>
  <tr>
    <td class="tg-0lax">ETH</td>
    <td class="tg-0lax">2018-01-15</td>
    <td class="tg-0lax">1,365.20996094</td>
    <td class="tg-0lax">1,291.92004395</td>
    <td class="tg-0lax">1,390.58996582</td>
    <td class="tg-0lax">4</td>
  </tr>
  <tr>
    <td class="tg-0lax">ETH</td>
    <td class="tg-0lax">2018-01-11</td>
    <td class="tg-0lax">1,268.08996582</td>
    <td class="tg-0lax">1,154.93005371</td>
    <td class="tg-0lax">1,337.30004883</td>
    <td class="tg-0lax">5</td>
  </tr>
  <tr>
    <td class="tg-0lax">XLM</td>
    <td class="tg-0lax">2018-01-04</td>
    <td class="tg-0lax">0.892399013</td>
    <td class="tg-0lax">0.7240499854</td>
    <td class="tg-0lax">0.9381440282</td>
    <td class="tg-0lax">1</td>
  </tr>
  <tr>
    <td class="tg-0lax">XLM</td>
    <td class="tg-0lax">2018-01-03</td>
    <td class="tg-0lax">0.5627200007</td>
    <td class="tg-0lax">0.8962270021</td>
    <td class="tg-0lax">0.9199939966</td>
    <td class="tg-0lax">2</td>
  </tr>
  <tr>
    <td class="tg-0lax">XLM</td>
    <td class="tg-0lax">2018-01-05</td>
    <td class="tg-0lax">0.7394570112</td>
    <td class="tg-0lax">0.662711978</td>
    <td class="tg-0lax">0.7919669747</td>
    <td class="tg-0lax">3</td>
  </tr>
  <tr>
    <td class="tg-0lax">XLM</td>
    <td class="tg-0lax">2018-01-06</td>
    <td class="tg-0lax">0.6630600095</td>
    <td class="tg-0lax">0.7099450231</td>
    <td class="tg-0lax">0.7885069847</td>
    <td class="tg-0lax">4</td>
  </tr>
  <tr>
    <td class="tg-0lax">XLM</td>
    <td class="tg-0lax">2018-01-07</td>
    <td class="tg-0lax">0.7097300291</td>
    <td class="tg-0lax">0.6996449828</td>
    <td class="tg-0lax">0.7447350025</td>
    <td class="tg-0lax">5</td>
  </tr>
  <tr>
    <td class="tg-0lax">XRP</td>
    <td class="tg-0lax">2018-01-04</td>
    <td class="tg-0lax">3.1173400879</td>
    <td class="tg-0lax">3.1966300011</td>
    <td class="tg-0lax">3.8419399261</td>
    <td class="tg-0lax">1</td>
  </tr>
  <tr>
    <td class="tg-0lax">XRP</td>
    <td class="tg-0lax">2018-01-05</td>
    <td class="tg-0lax">3.3008100986</td>
    <td class="tg-0lax">3.0487101078</td>
    <td class="tg-0lax">3.5646800995</td>
    <td class="tg-0lax">2</td>
  </tr>
  <tr>
    <td class="tg-0lax">XRP</td>
    <td class="tg-0lax">2018-01-07</td>
    <td class="tg-0lax">3.0929598808</td>
    <td class="tg-0lax">3.3778100014</td>
    <td class="tg-0lax">3.4872500896</td>
    <td class="tg-0lax">3</td>
  </tr>
  <tr>
    <td class="tg-0lax">XRP</td>
    <td class="tg-0lax">2018-01-08</td>
    <td class="tg-0lax">3.3635699749</td>
    <td class="tg-0lax">2.456209898</td>
    <td class="tg-0lax">3.3635699749</td>
    <td class="tg-0lax">4</td>
  </tr>
  <tr>
    <td class="tg-0lax">XRP</td>
    <td class="tg-0lax">2018-01-03</td>
    <td class="tg-0lax">2.464099884</td>
    <td class="tg-0lax">3.1053700447</td>
    <td class="tg-0lax">3.2793800831</td>
    <td class="tg-0lax">5</td>
  </tr>
</table>