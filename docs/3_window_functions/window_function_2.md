# Example 2

## Query

```sql
select
    symbol
   ,date_time
   ,"open"
   ,"close"
   ,rank() over (partition by symbol order by market_cap desc) as day_rank
from crypto_prices
where symbol = 'BTC'
limit 10;
```

## Results

<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg .tg-0lax{text-align:left;vertical-align:top}
</style>
<table class="tg">
  <tr>
    <th class="tg-0lax">symbol</th>
    <th class="tg-0lax">date_time</th>
    <th class="tg-0lax">open</th>
    <th class="tg-0lax">close</th>
    <th class="tg-0lax">day_rank</th>
  </tr>
  <tr>
    <td class="tg-0lax">BTC</td>
    <td class="tg-0lax">2017-12-17</td>
    <td class="tg-0lax">19,475.8007812</td>
    <td class="tg-0lax">19,140.8007812</td>
    <td class="tg-0lax">1</td>
  </tr>
  <tr>
    <td class="tg-0lax">BTC</td>
    <td class="tg-0lax">2017-12-19</td>
    <td class="tg-0lax">19,118.3007812</td>
    <td class="tg-0lax">17,776.6992188</td>
    <td class="tg-0lax">2</td>
  </tr>
  <tr>
    <td class="tg-0lax">BTC</td>
    <td class="tg-0lax">2017-12-18</td>
    <td class="tg-0lax">19,106.4003906</td>
    <td class="tg-0lax">19,114.1992188</td>
    <td class="tg-0lax">3</td>
  </tr>
  <tr>
    <td class="tg-0lax">BTC</td>
    <td class="tg-0lax">2017-12-08</td>
    <td class="tg-0lax">17,802.9003906</td>
    <td class="tg-0lax">16,569.4003906</td>
    <td class="tg-0lax">4</td>
  </tr>
  <tr>
    <td class="tg-0lax">BTC</td>
    <td class="tg-0lax">2017-12-20</td>
    <td class="tg-0lax">17,760.3007812</td>
    <td class="tg-0lax">16,624.5996094</td>
    <td class="tg-0lax">5</td>
  </tr>
  <tr>
    <td class="tg-0lax">BTC</td>
    <td class="tg-0lax">2017-12-16</td>
    <td class="tg-0lax">17,760.3007812</td>
    <td class="tg-0lax">19,497.4003906</td>
    <td class="tg-0lax">6</td>
  </tr>
  <tr>
    <td class="tg-0lax">BTC</td>
    <td class="tg-0lax">2018-01-07</td>
    <td class="tg-0lax">17,527.3007812</td>
    <td class="tg-0lax">16,477.5996094</td>
    <td class="tg-0lax">7</td>
  </tr>
  <tr>
    <td class="tg-0lax">BTC</td>
    <td class="tg-0lax">2018-01-06</td>
    <td class="tg-0lax">17,462.0996094</td>
    <td class="tg-0lax">17,527.0000</td>
    <td class="tg-0lax">8</td>
  </tr>
  <tr>
    <td class="tg-0lax">BTC</td>
    <td class="tg-0lax">2017-12-13</td>
    <td class="tg-0lax">17,500.0000</td>
    <td class="tg-0lax">16,408.1992188</td>
    <td class="tg-0lax">9</td>
  </tr>
  <tr>
    <td class="tg-0lax">BTC</td>
    <td class="tg-0lax">2017-12-12</td>
    <td class="tg-0lax">16,919.8007812</td>
    <td class="tg-0lax">17,415.4003906</td>
    <td class="tg-0lax">10</td>
  </tr>
</table>