# Columns and Datatypes

- `id` `int` - primary key
- `coin_id` `int` - a unique id for each different symbol
- `symbol` `text` - the asset symbol, 'BTC', 'ETH'
- `date_time` `date` - day date
- `open` `float` - opening price of the day
- `high` `float` - high price
- `low` `float` - low price
- `close` `float` - closing price
- `volume` `float` - total value of all trades
- `market_cap` `bigint` - total value of that asset.

## Data Sample

<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:10px;padding:3px 3px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg th{font-family:Arial, sans-serif;font-size:10px;font-weight:normal;padding:3px 3px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg .tg-0lax{text-align:left;vertical-align:top}
</style>
<table class="tg">
  <tr>
    <th class="tg-0lax">id</th>
    <th class="tg-0lax">coin_id</th>
    <th class="tg-0lax">symbol</th>
    <th class="tg-0lax">date_time</th>
    <th class="tg-0lax">open</th>
    <th class="tg-0lax">high</th>
    <th class="tg-0lax">low</th>
    <th class="tg-0lax">close</th>
    <th class="tg-0lax">volume</th>
    <th class="tg-0lax">market_cap</th>
  </tr>
  <tr>
    <td class="tg-0lax">833,637</td>
    <td class="tg-0lax">1</td>
    <td class="tg-0lax">BTC</td>
    <td class="tg-0lax">2018-08-04</td>
    <td class="tg-0lax">7,438.6700</td>
    <td class="tg-0lax">7,497.4900</td>
    <td class="tg-0lax">6,984.0700</td>
    <td class="tg-0lax">7,032.8500</td>
    <td class="tg-0lax">4,268,390,000.0000</td>
    <td class="tg-0lax">127,859,350,021</td>
  </tr>
  <tr>
    <td class="tg-0lax">833,638</td>
    <td class="tg-0lax">1</td>
    <td class="tg-0lax">BTC</td>
    <td class="tg-0lax">2018-08-03</td>
    <td class="tg-0lax">7,562.1400</td>
    <td class="tg-0lax">7,562.1400</td>
    <td class="tg-0lax">7,328.6500</td>
    <td class="tg-0lax">7,434.3900</td>
    <td class="tg-0lax">4,627,150,000.0000</td>
    <td class="tg-0lax">129,965,898,111</td>
  </tr>
  <tr>
    <td class="tg-0lax">833,639</td>
    <td class="tg-0lax">1</td>
    <td class="tg-0lax">BTC</td>
    <td class="tg-0lax">2018-08-02</td>
    <td class="tg-0lax">7,634.1900</td>
    <td class="tg-0lax">7,712.7700</td>
    <td class="tg-0lax">7,523.4400</td>
    <td class="tg-0lax">7,567.1500</td>
    <td class="tg-0lax">4,214,110,000.0000</td>
    <td class="tg-0lax">131,189,721,712</td>
  </tr>
  <tr>
    <td class="tg-0lax">833,640</td>
    <td class="tg-0lax">1</td>
    <td class="tg-0lax">BTC</td>
    <td class="tg-0lax">2018-08-01</td>
    <td class="tg-0lax">7,769.0400</td>
    <td class="tg-0lax">7,769.0400</td>
    <td class="tg-0lax">7,504.9500</td>
    <td class="tg-0lax">7,624.9100</td>
    <td class="tg-0lax">4,797,620,000.0000</td>
    <td class="tg-0lax">133,492,645,169</td>
  </tr>
  <tr>
    <td class="tg-0lax">833,641</td>
    <td class="tg-0lax">1</td>
    <td class="tg-0lax">BTC</td>
    <td class="tg-0lax">2018-07-31</td>
    <td class="tg-0lax">8,181.2000</td>
    <td class="tg-0lax">8,181.5300</td>
    <td class="tg-0lax">7,696.9300</td>
    <td class="tg-0lax">7,780.4400</td>
    <td class="tg-0lax">5,287,530,000.0000</td>
    <td class="tg-0lax">140,559,547,538</td>
  </tr>
  <tr>
    <td class="tg-0lax">833,642</td>
    <td class="tg-0lax">1</td>
    <td class="tg-0lax">BTC</td>
    <td class="tg-0lax">2018-07-30</td>
    <td class="tg-0lax">8,221.5800</td>
    <td class="tg-0lax">8,235.5000</td>
    <td class="tg-0lax">7,917.5000</td>
    <td class="tg-0lax">8,180.4800</td>
    <td class="tg-0lax">5,551,400,000.0000</td>
    <td class="tg-0lax">141,239,444,979</td>
  </tr>
  <tr>
    <td class="tg-0lax">833,643</td>
    <td class="tg-0lax">1</td>
    <td class="tg-0lax">BTC</td>
    <td class="tg-0lax">2018-07-29</td>
    <td class="tg-0lax">8,205.8203125</td>
    <td class="tg-0lax">8,272.25976562</td>
    <td class="tg-0lax">8,141.18017578</td>
    <td class="tg-0lax">8,218.45996094</td>
    <td class="tg-0lax">4,107,190,016.0000</td>
    <td class="tg-0lax">140,951,371,776</td>
  </tr>
  <tr>
    <td class="tg-0lax">833,644</td>
    <td class="tg-0lax">1</td>
    <td class="tg-0lax">BTC</td>
    <td class="tg-0lax">2018-07-28</td>
    <td class="tg-0lax">8,169.06005859</td>
    <td class="tg-0lax">8,222.84960938</td>
    <td class="tg-0lax">8,110.77001953</td>
    <td class="tg-0lax">8,192.15039062</td>
    <td class="tg-0lax">3,988,750,080.0000</td>
    <td class="tg-0lax">140,305,137,664</td>
  </tr>
  <tr>
    <td class="tg-0lax">833,645</td>
    <td class="tg-0lax">1</td>
    <td class="tg-0lax">BTC</td>
    <td class="tg-0lax">2018-07-27</td>
    <td class="tg-0lax">7,950.39990234</td>
    <td class="tg-0lax">8,262.66015625</td>
    <td class="tg-0lax">7,839.75976562</td>
    <td class="tg-0lax">8,165.00976562</td>
    <td class="tg-0lax">5,195,879,936.0000</td>
    <td class="tg-0lax">136,532,811,776</td>
  </tr>
  <tr>
    <td class="tg-0lax">833,646</td>
    <td class="tg-0lax">1</td>
    <td class="tg-0lax">BTC</td>
    <td class="tg-0lax">2018-07-26</td>
    <td class="tg-0lax">8,176.85009766</td>
    <td class="tg-0lax">8,290.33007812</td>
    <td class="tg-0lax">7,878.70996094</td>
    <td class="tg-0lax">7,951.58007812</td>
    <td class="tg-0lax">4,899,089,920.0000</td>
    <td class="tg-0lax">140,404,998,144</td>
  </tr>
</table>
