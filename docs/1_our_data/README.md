# Sample Data

The content of the database comes from a [CSV I found on reddit with 5 years of crypto prices](https://www.reddit.com/r/CryptoCurrency/comments/988lxb/i_have_5_years_of_coinmarketcapcom_crypto_price/) from [www.coinmarketcap.com](https://www.coinmarketcap.com).

Five years of daily:

- open
- high
- low
- close
- volume
- market capitalization

for multiple blockchain/cryptocurrency assets.
