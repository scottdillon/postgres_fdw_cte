# How Much Data

## Rows - 835,561

## Cryptocurrencies - 1,727

## A Sample

| symbol | symbol_rows |   max_market_cap    |
| :----: | :---------: | :-----------------: |
|  BTC   |    1925     | $326,141,280,256.00 |
|  ETH   |    1094     | $135,503,314,944.00 |
|  XRP   |    1827     | $130,301,689,856.00 |
|  BCH   |     378     | $65,933,676,544.00  |
|  ADA   |     308     | $30,364,432,384.00  |
|  LTC   |    1925     | $19,525,496,832.00  |
|  EOS   |     400     | $17,849,051,136.00  |
|  XEM   |    1222     | $16,520,262,656.00  |
|  XLM   |    1461     | $15,953,612,800.00  |
| MIOTA  |     418     | $14,929,008,640.00  |

Query:

```sql
select
	symbol
   ,count(open) as symbol_rows
   ,cast(max(c.market_cap) as money) as max_market_cap
from public.crypto_prices as c
group by symbol
order by max(c.market_cap) desc
limit 10;
```
