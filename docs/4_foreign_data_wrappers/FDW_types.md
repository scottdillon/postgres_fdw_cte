# Are there other kinds of data wrappers?

- Postgres
- [SQL Alchemy](https://github.com/Kozea/sqlalchemy_fdw)(`SELECT` only)
- [`MySQL`](https://www.mysql.com/)
- [`Oracle`](https://github.com/laurenz/oracle_fdw)
- [`SQLite`](https://github.com/pgspider/sqlite_fdw)
- [`JSON`](https://github.com/nkhorman/json_fdw)
- [`XML`](https://github.com/Kozea/Multicorn)
- [FileSystem](https://multicorn.readthedocs.io/en/latest/foreign-data-wrappers/fsfdw.html)
- [docker containers](https://github.com/paultag/dockerfdw)
- [Cassandra](https://github.com/wjch-krl/pgCassandra)
- [MongoDB](https://github.com/EnterpriseDB/mongo_fdw)
- [Redis](https://github.com/pg-redis-fdw/redis_fdw)
