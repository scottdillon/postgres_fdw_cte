# SQL Alchemy Example

```sql
CREATE SERVER alchemy_srv foreign data wrapper multicorn options (
    wrapper 'multicorn.sqlalchemyfdw.SqlAlchemyFdw'
);

create foreign table mysql_table (
  column1 integer,
  column2 varchar
) server alchemy_srv options (
  tablename 'table',
  db_url 'mysql://myuser:mypassword@myhost/mydb'
);
```