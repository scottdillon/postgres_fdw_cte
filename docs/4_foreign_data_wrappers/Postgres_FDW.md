# Postgres FDW

- Access a table in another postgresql database as if it was in your database.
- No copying of data necessary.
- Can import an entire schema in bulk.

    ```
    IMPORT FOREIGN SCHEMA "public" FROM SERVER hr INTO public;
    ```

- Can selectively import tables.

    ```
    IMPORT FOREIGN SCHEMA "public" limit to (employee) FROM SERVER hr INTO public;
    ```

## More Info on Postgres FDW
[Thorough Review of Postgres FDW](https://www.percona.com/blog/2018/08/21/foreign-data-wrappers-postgresql-postgres_fdw/)