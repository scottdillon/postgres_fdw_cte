# Foreign Data Wrappers

- Access external data sources as if they were part of your local schema.
- \>= Postgres 9.1
- With limitations
  - Still depends on permissions of user.
  - Some FDW do not have more than `SELECT` functionality.
  - Often the documentation is .... lacking.
  - Some only work with one version of Postgres.
  - Some only work with a non-master branch of a dependency.

## What comes with Postgres?

- `PostgreSQL` Data Wrapper
- `File` Data Wrapper (Any format read by [`COPY`](https://www.postgresql.org/docs/10/static/sql-copy.html))
  - Text
  - CSV
  - Binary

## Overviews of FDW

[A Tour of Postgres Foreign Data Wrappers by Percona](http://www.craigkerstiens.com/2016/09/11/a-tour-of-fdws/)
