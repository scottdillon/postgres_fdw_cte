# Other FDWs

## Other Lists

[PostgreSQL.org List of FDW](https://wiki.postgresql.org/wiki/Foreign_data_wrappers)

- Git
- Ical
- IMAP
- RSS
- www - Allows querying different web services
- Google
- S3
- Twitter
- Google Spreadsheets
- Apache Hive
- HDFS
- Hadoop
- BigQuery
- unix-type log files
- Phillips Hue Lighting

[Multicorn 3rd Party Wrappers](https://multicorn.readthedocs.io/en/latest/third-party-fdw.html)

- docker containers, images, etc
- Apache Hive
- facebook
- OpenStack/Telemetry
- S3 CSV - Reads from CSV files stored on S3
- S3 JSON - Reads from JSON files stored on S3
