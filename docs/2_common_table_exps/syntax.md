# General Syntax

```sql
WITH <cte_query_1_name> AS (
    CTE Query 1
)[, <cte_query_2_name> AS (
    CTE Query 2
)]...

SELECT
    *
FROM table_1, <cte_query_1_name>,...
```

CTE query 2 can use CTE query 1 in FROM clause.
