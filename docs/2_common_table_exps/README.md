# Common Table Expressions

## What is a [Common Table Expression](https://www.postgresql.org/docs/10/static/queries-with.html)?

- Common Table Expression, CTE or `WITH` query.
- A per query table created just for that query.
- Recreated every time the query is run.
- `WITH` clause can contain a `SELECT`, `INSERT`, `UPDATE`, or `DELETE` query.
- a `WITH` clause attaches to a primary statement which itself can contain `SELECT`, `INSERT`, `UPDATE`, or `DELETE` statements.

## Uses

- Multiple Functions in Same Query
- Recursive Queries
- Replace subquery for clarity

## More Info

[The Best Postgres Feature You're Not Using](http://www.craigkerstiens.com/2013/11/18/best-postgres-feature-youre-not-using/)
[Advanced SQL - Common Table Expressions](http://mjk.space/advanced-sql-cte/)