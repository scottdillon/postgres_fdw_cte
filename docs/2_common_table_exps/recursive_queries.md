# Recursive Queries

<pre>
WITH RECURSIVE included_parts(sub_part, part, quantity) AS (
    SELECT sub_part, part, quantity FROM parts WHERE part = 'our_product'
    UNION ALL<span style="color:red">
    SELECT
        p.sub_part
       ,p.part
       ,p.quantity
    FROM included_parts pr, parts p
    WHERE p.part = pr.sub_part</span>
)
SELECT
   ,sub_part
   ,SUM(quantity) as total_quantity
FROM included_parts
GROUP BY sub_part
</pre>

## Recursive CTE Summary
[Recursive CTE Review](http://www.postgresqltutorial.com/postgresql-recursive-query/)