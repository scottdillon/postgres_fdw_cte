select 
	-- ROW_NUMBER, RANK, DENSE_RANK funcs available
	-- Rank the days with highest BTC open price
	rank()
   	    over (
   	    	partition by symbol
   	    	order by open desc
   	    	)
   ,symbol
   ,date_time
   ,open
   ,high
   ,low
   ,close
from crypto_prices
where symbol = 'BTC'
order by rank;

select 
	row_number()
   	    over (
   	    	partition by symbol
   	    	order by date_time
   	    	)
   ,symbol
   ,date_time
   ,open
   ,high
   ,low
   ,close
from crypto_prices
where symbol = 'BTC'
order by row_number desc;

-- What is the average price per week of BTC.
select 
	round(
		avg(open)
   	    	over (
   	    		partition by symbol, 
   	    		date_part('year', date_time),
   	    		date_part('week', date_time)
   	    	)::numeric, 2) as average
   ,symbol
   ,date_part('year', date_time)::integer as year
   ,date_part('week', date_time)::integer as week_number
from crypto_prices
where symbol = 'BTC'
group by 
	date_time
    ,symbol
	,date_part('year', date_time)::integer 
    ,date_part('week', date_time)::integer
    ,open
order by date_part('year', date_time)::integer,date_part('week', date_time)::integer desc;