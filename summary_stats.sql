select count(*) 
from public.crypto_prices;

select count(distinct(crypto_prices.symbol)) 
from public.crypto_prices;

select 
	symbol
   ,count(open) as symbol_rows
   ,cast(max(c.market_cap) as money) as max_market_cap
from public.crypto_prices as c
group by symbol
order by max(c.market_cap) desc
limit 10;


select 
	-- ROW_NUMBER, RANK, DENSE_RANK funcs available
	-- Rank the days with highest BTC open price
	rank()
   	    over (
   	    	partition by symbol
   	    	order by open desc
   	    	)
   ,symbol
   ,date_time
   ,open
   ,high
   ,low
   ,close
from crypto_prices
where symbol = 'BTC'
order by rank;