# PostgreSQL Common Table Expressions, Window Functions and Foreign Data Wrappers

This repo uses [gitbook](https://toolchain.gitbook.com/setup.html) to display it's content.

[gitbook structure](https://toolchain.gitbook.com/structure.html)

The content of the database comes from a [CSV I found on reddit with 5 years of crypto prices](https://www.reddit.com/r/CryptoCurrency/comments/988lxb/i_have_5_years_of_coinmarketcapcom_crypto_price/) from [www.coinmarketcap.com](https://www.coinmarketcap.com).
