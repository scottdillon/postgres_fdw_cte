create extension file_fdw;
create server crypto_csv foreign data WRAPPER file_fdw;
drop foreign table if exists public.crypto_prices;
create foreign table public.crypto_prices (
	id int
   ,coin_id int
   ,symbol text
   ,date_time date
   ,open float
   ,high float
   ,low float
   ,close float
   ,volume float
   ,market_cap bigint
	) SERVER crypto_csv
options (
    -- csv format allows use of header option for csv files.
	format 'csv',
	filename '/csv/crypto_data_5_years.csv',
	delimiter ',',
	null '',
	header 'true'
	);